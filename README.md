# Install parser

+ Make DB dump
  + **File:** DBdump/parser.sql
+ Make configuration (db, redis, parse config ...)
    + **File:** .env
+ Run command (for uploading ip to redis)
  + php public/uploade-ips.php


**Start parser:** php public/index.php

**For reload urls from error:** php public/index.php from-errors
