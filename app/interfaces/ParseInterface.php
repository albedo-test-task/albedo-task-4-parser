<?php

namespace App\interfaces;

interface ParseInterface {
    public function runUrl(): void;
}