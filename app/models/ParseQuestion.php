<?php

namespace App\models;

use App\interfaces\ParseInterface;
use App\models\RedisModel;
use DiDom\Document;
use App\models\HttpClient;

class ParseQuestion implements ParseInterface
{
    public final const CHILD_CLASS = 'ParseAnswer';

    private string $url;
    protected HttpClient $parseClient;
    protected $redisConnection;
    protected $mysqlConnection;

    public function __construct($url, $redis = null, $mysql = null)
    {
        $this->url = $_ENV['PARSER_BASE_URL'].$url;
        $this->parseClient = new HttpClient($this->url);
        $this->redisConnection = $redis;
        $this->mysqlConnection = $mysql;
    }

    /**
     * @return void
     * @throws \DiDom\Exceptions\InvalidSelectorException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function runUrl(): void
    {
        $pageDom = $this->parseClient->getPage();

        /** @var TYPE_NAME $pageDom */
        $question = $pageDom->find('h1 span.Normal')[0]->text();
        $saveQuestion = DbModel::saveQuestion($question, $this->mysqlConnection);

        if ($saveQuestion === -1) {
            // Back to queue with errors
            Queue::putInQueue(
                str_replace($_ENV['PARSER_BASE_URL'], "", $this->url),
                (new \ReflectionClass($this))->getShortName(),
                Queue::QUEUE_ERRORS_NAME
            );
        } elseif ($saveQuestion) {
            // Check answers
            $tableStrings = $pageDom->find('table#kxo tbody tr');

            foreach ($tableStrings as $tableString) {
                $answerText = $tableString->find('td.Answer a')[0]->text();
                $answerTextLength = $tableString->find('td.Length')[0]->text();
                $answerUrl = $tableString->find('td.Answer a')[0]->attributes()['href'];

                $checkAnswer = DbModel::checkAnswer(
                    $answerText,
                    $saveQuestion,
                    $answerTextLength,
                    $this->mysqlConnection);
                if($checkAnswer == 0) Queue::putInQueue($answerUrl,self::CHILD_CLASS);
            }
        }
    }
}