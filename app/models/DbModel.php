<?php

namespace App\models;

use App\models\Logger;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

class DbModel
{
    public Capsule $connection;

    public function __construct()
    {
        $this->connection = self::connect();
    }

    /**
     * @return Capsule
     */
    private static function connect(): Capsule
    {
        $capsule = new Capsule;
        $capsule->addConnection([
            'driver' => $_ENV['DB_CONNECTION'],
             'host' =>  $_ENV['DB_HOST'],
             'database' =>  $_ENV['DB_DATABASE'],
             'username' =>  $_ENV['DB_USERNAME'],
             'password' => $_ENV['DB_PASSWORD'],
             'charset' => 'utf8',
             'collation' => 'utf8_unicode_ci',
             'prefix' => '',
        ]);
        $capsule->setEventDispatcher(new Dispatcher(new Container));
        $capsule->setAsGlobal();

        return $capsule;
    }

    /**
     * @return Capsule
     */
    public static function createMultiConnect(): Capsule
    {
        return self::connect();
    }

    /**
     *   0 - Question already exists
     *  -1 - Save error
     * > 0 - Question was saved. Return question id
     *
     * @param $question
     * @param $connection
     * @return int
     */
    public static function saveQuestion($question, $connection = null): int
    {
        if(!$connection) {
            $connection = self::createMultiConnect();
        }

        // Check exists question
        $dbQuestion = $connection->table('questions')
            ->where('question', '=', $question)
            ->limit(1)
            ->get();

        if($dbQuestion->isEmpty()) {
            $newQuestionId = null;

            try {
                $newQuestionId = $connection->table('questions')
                    ->insertGetId(values: ["question" => $question]);
            } catch (Throwable $t) {
                Logger::log("DB error. Question is not saved", "error", $t->getMessage());
            }

            return $newQuestionId ? $newQuestionId : -1;
        } else {
            return 0;
        }
    }

    /**
     *   0 - Answer not exist
     * > 0 - Answer exist. Return answer id
     * @param $answer
     * @param $questionId
     * @param  null  $lenght
     * @param  null  $connection
     * @return int
     */
    public static function checkAnswer($answer, $questionId, $lenght = null,  $connection = null): int
    {
        if(!$connection) {
            $connection = self::createMultiConnect();
        }

        // Check exists answer
        $dbAnswer = $connection->table('answers')
            ->where('answer', '=', $answer)
            ->limit(1)
            ->get();

        if ($dbAnswer->isEmpty()) {
            return 0;
        } else {
            if(!$dbAnswer[0]->length) {
                try {
                    $connection->table('answers')
                        ->where('id', '=', $dbAnswer[0]->id)
                        ->update(['length' => $lenght]);
                } catch (Throwable $t) {
                    Logger::log("DB error. Answer is not updated", "error", $t->getMessage());
                }
            }

            self::makeBinding($questionId, $dbAnswer[0]->id);

            return $dbAnswer[0]->id;
        }
    }

    /**
     *   0 - Question not exist
     * > 0 - Question exist. Return question id
     * @param $question
     * @param $answerId
     * @param  null  $connection
     * @return int
     */
    public static function checkQuestion($question, $answerId, $connection = null): int
    {
        if(!$connection) {
            $connection = self::createMultiConnect();
        }

        // Check exists question
        $dbQuestion = $connection->table('questions')
            ->where('question', '=', $question)
            ->limit(1)
            ->get();

        if ($dbQuestion->isEmpty()) {
            return 0;
        } else {
            self::makeBinding($dbQuestion[0]->id, $answerId);
            return $dbQuestion[0]->id;
        }
    }

    /**
     * @param $questionId
     * @param $answerId
     * @param  null  $connection
     * @return void
     */
    protected static function makeBinding($questionId, $answerId, $connection = null): void
    {
        if(!$connection) {
            $connection = self::createMultiConnect();
        }

        try {
            $binding = $connection->table('answer_to_question')
                ->where(['answer_id' => $answerId, 'question_id' => $questionId])
                ->limit(1)
                ->get();

            if ($binding->isEmpty()) {
                $connection->table('answer_to_question')
                    ->insert(values: ['answer_id' => $answerId, 'question_id' => $questionId]);
            }
        } catch (Throwable $t) {
            Logger::log("DB error. Bind is not created", "error", $t->getMessage());
        }

    }

    /**
     *   0 - Answer already exists
     *  -1 - Save error
     * > 0 - Answer was saved. Return answer id
     * @param $answer
     * @param  null  $connection
     * @return int
     */
    public static function saveAnswer($answer, $connection = null): int
    {
        if(!$connection) {
            $connection = self::createMultiConnect();
        }

        // Check exists answer
        $dbAnswer = $connection->table('answers')
            ->where('answer', '=', $answer)
            ->limit(1)
            ->get();

        if($dbAnswer->isEmpty()) {
            $newAnswerId = null;
            try {
                $newAnswerId = $connection->table('answers')
                    ->insertGetId(values: ["answer" => $answer]);
            } catch (Throwable $t) {
                Logger::log("DB error. Answer is not saved.", "error", $t->getMessage());
            }

            return $newAnswerId ? $newAnswerId : -1;
        } else {
            return 0;
        }
    }
}
