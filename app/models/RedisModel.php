<?php

namespace App\models;

use Predis\Client;

class RedisModel
{
    public $connection;

    public function __construct()
    {
        $this->connection = self::connect();
    }

    private static function connect()
    {
        \Predis\Autoloader::register();

        return new Client([
            'scheme' => 'tcp',
            'host' => $_ENV['REDIS_HOST'],
            'port' => $_ENV['REDIS_PORT'],
        ]);
    }

    /**
     * @return Client
     */
    public static function createMultiConnect()
    {
        return self::connect();
    }
}