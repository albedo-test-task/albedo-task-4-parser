<?php

namespace App\models;

use GuzzleHttp\Client;
use DiDom\Document;

class HttpClient
{
    private $url;
    private $proxy;

    public function __construct($url)
    {
        $this->url = $url;
        $redis = RedisModel::createMultiConnect();
        $this->proxy = "http://172.17.0.1:8118";
    }

    /**
     * @return Document
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getPage()
    {
        $document = null;

        try {
            $client = new Client();
            $response = $client->request('GET', $this->url, ['proxy' => $this->proxy]);
            $html = $response->getBody();
            $document = new Document();
            $document->loadHtml($html);
        } catch (GuzzleException $e) {
            Logger::log("HttpClient error. Couldn't make request", "error", $e->getMessage());
        }

        return $document;
    }
}