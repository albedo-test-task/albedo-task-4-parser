<?php

namespace App\models;

use App\interfaces\ParseInterface;
use App\models\RedisModel;
use DiDom\Document;
use App\models\HttpClient;
use GuzzleHttp\Exception\GuzzleException;

class ParseLeterNavigation implements ParseInterface
{
    public final const CHILD_CLASS = 'ParseLeterTable';

    private string $url;
    protected HttpClient $parseClient;

    public function __construct($url)
    {
        $this->url = $_ENV['PARSER_BASE_URL'].$url;
        $this->parseClient = new HttpClient($this->url);
    }

    /**
     * @return void
     * @throws GuzzleException
     */
    public function runUrl(): void
    {
        $pageDom = $this->parseClient->getPage();

        /** @var TYPE_NAME $pageDom */
        $urls = $pageDom->find('ul.dnrg a');

        foreach ($urls as $key => $url) {
            $urls[$key] = $url->attributes()['href'];
            Queue::putInQueue($urls[$key],self::CHILD_CLASS);
        }
    }
}