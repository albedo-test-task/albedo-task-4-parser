<?php

namespace App\models;

use Monolog\Handler\StreamHandler;
use Monolog\Level;
use Monolog\Logger as MLogger;

class Logger
{
    /**
     * @param $textError
     * @param $logType
     * @param $data
     * @return void
     */
    public static function log($textError, $logType, $data = null)
    {
        // Log in file
        $log = new MLogger('parse_logs');
        $log->pushHandler(new StreamHandler('storage/logs/logs.log', Level::Warning));
        $log->$logType($textError, $data);

        // Log in command string
        print $textError . "\n";
        print_r($data);
        print "\n\n";
    }
}