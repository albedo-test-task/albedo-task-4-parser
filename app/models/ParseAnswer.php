<?php

namespace App\models;

use App\interfaces\ParseInterface;
use App\models\HttpClient;
use App\models\RedisModel;
use DiDom\Document;
use GuzzleHttp\Exception\GuzzleException;

class ParseAnswer implements ParseInterface
{
    public final const CHILD_CLASS = 'ParseQuestion';

    private string $url;
    protected HttpClient $parseClient;
    protected $redisConnection;
    protected $mysqlConnection;

    public function __construct($url, $redis = null, $mysql = null)
    {
        $this->url = $_ENV['PARSER_BASE_URL'].$url;
        $this->parseClient = new HttpClient($this->url);
        $this->redisConnection = $redis;
        $this->mysqlConnection = $mysql;
    }

    /**
     * @return void
     */
    public function runUrl(): void
    {
        $pageDom = $this->parseClient->getPage();

        $answer = $pageDom->find('h1 span.Normal')[0]->text();
        $saveAnswer = DbModel::saveAnswer($answer, $this->mysqlConnection);

        if ($saveAnswer === -1) {
            // Back to queue with errors
            Queue::putInQueue(
                str_replace($_ENV['PARSER_BASE_URL'], "", $this->url),
                (new \ReflectionClass($this))->getShortName(),
                Queue::QUEUE_ERRORS_NAME
            );
        } elseif ($saveAnswer) {
            // Check question
            $tableStrings = $pageDom->find('div.ContentElement table tbody tr');

            foreach ($tableStrings as $tableString) {
                $questionText = $tableString->find('td.QuestionShort a')[0]->text();
                $questionUrl = $tableString->find('td.QuestionShort a')[0]->attributes()['href'];

                $checkQuestion = DbModel::checkQuestion($questionText, $saveAnswer, $this->mysqlConnection);
                if($checkQuestion == 0) Queue::putInQueue($questionUrl,self::CHILD_CLASS);
            }
        }
    }
}