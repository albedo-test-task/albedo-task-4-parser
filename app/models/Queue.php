<?php

namespace App\models;

use App\models\RedisModel;
use DiDom\Exceptions\InvalidSelectorException;
use GuzzleHttp\Exception\GuzzleException;

class Queue
{
    const QUEUE_NAME = 'queue';
    const QUEUE_ERRORS_NAME = 'queue-errors';

    /**
     * @return void
     */
    public static function startQueue(): void
    {
        $processNumber = $_ENV['PROCESS_COUNT'];
        $processes = [];

        $redis = RedisModel::createMultiConnect();
        $mysql = DbModel::createMultiConnect();

        // Queue cycle
        while ($redis->scard(self::QUEUE_NAME) || count($processes)) {
            if (count($processes)) {
                foreach ($processes as $process) {
                    $childProcessId = pcntl_waitpid($process, $status, WNOHANG);
                    if ($childProcessId === -1) {
                        print "Error process id: $process \n";
                    } elseif ($childProcessId === 0) {
                        continue;
                    } else {
                        unset($processes[$process]);
                    }
                }
            }

            if (count($processes) >= $processNumber) continue;

            $pid = pcntl_fork();
            if ($pid == -1) {
                die('Could not make fork ... \n');
            } else if ($pid) {
                // parent process
                $processes[$pid] = $pid;
            } else {
                // child process
                $redis = RedisModel::createMultiConnect();
                $mysql = DbModel::createMultiConnect();
                break;
            }
        }

        while ($redis->scard(self::QUEUE_NAME)) {
            $element = $redis->spop(self::QUEUE_NAME);
            if (!$element) {
                break;
            }

            $data = explode("|", $element);
            $class = '\\App\\models\\' . $data[1];
            if (class_exists($class)) {
                $parserObject = new $class($data[0], $redis, $mysql);
                $parserObject->runUrl();
            } else {
                $redis->sadd(Queue::QUEUE_ERRORS_NAME, $element);
                print "Class " . $class . " not exists ... \n";
            }
        }
    }

    /**
     * If queue is empty - return TRUE
     * @return bool
     */
    public static function isEmptyQueue(): bool
    {
        $redis = RedisModel::createMultiConnect();
        return $redis->scard(self::QUEUE_NAME) ? false : true;
    }

    /**
     * @param  $url
     * @param  $class
     * @param  string  $type
     * @return int
     */
    public static function putInQueue($url, $class, string $type = self::QUEUE_NAME): int
    {
        $redis = RedisModel::createMultiConnect();
        $queueDate = $url . "|" . $class;
        return $redis->sadd($type, $queueDate);
    }

    /**
     * @return false|string
     * @throws GuzzleException|InvalidSelectorException
     */
    public static function findUrlClass(): bool|string
    {
        $url = $_ENV['PARSER_START_LINK'];

        if ($url == "uebersicht.html") {
            return "ParseLetersNavigation";
        } elseif (str_contains($url, "frage/")) {
            return "ParseQuestion";
        } elseif (str_contains($url, "antwort/")) {
            return "ParseAnswer";
        } elseif ($url == "uebersicht-zeichen.html") {
            return "ParseNumbersNavigation";
        }

        $parseClient = new HttpClient($_ENV['PARSER_BASE_URL'] . $url);
        $pageDom = $parseClient->getPage();

        $pageH2 = $pageDom->find('h2')[0]->text();
        $ulElement = $pageDom->find('ul.dnrg');

        // Check if is ParseLeterNavigation
        if (
            $pageH2
            && str_contains($pageH2, "Fragestellungen mit dem Anfangsbuchstaben:")
            && count($ulElement)
        ) {
            return "ParseLeterNavigation";
        }

        //  Check if is ParseLeterTable
        if (
            $pageH2
            && str_contains($pageH2, "Fragestellungen mit dem Anfangsbuchstaben:")
        ) {
            return "ParseLeterTable";
        }

        return false;
    }

    /**
     * Back urls from errors to queue
     * @return void
     */
    public static function reloadFromErrors(): void
    {
        $redis = RedisModel::createMultiConnect();
        while ($redis->scard(self::QUEUE_ERRORS_NAME)) {
            $url = $redis->spop(self::QUEUE_ERRORS_NAME);
            $redis->sadd(self::QUEUE_NAME, $url);
        }
    }
}
