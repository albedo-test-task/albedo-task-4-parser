<?php

set_time_limit(0);

require __DIR__ . '/../vendor/autoload.php';

use Monolog\Level;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use App\models\DbModel;
use App\models\RedisModel;
use App\models\Queue;

Dotenv\Dotenv::createImmutable(__DIR__ . "/../")->load();

if (isset($argv[1]) && $argv[1] == "from-errors") Queue::reloadFromErrors();

if(Queue::isEmptyQueue()) {
    $class = Queue::findUrlClass();
    if(!$class) {
        print "Error - can't find url class\n";
        die;
    }

    Queue::putInQueue($_ENV['PARSER_START_LINK'], $class);
}

Queue::startQueue();
