<?php
require __DIR__ . '/../vendor/autoload.php';

use App\models\RedisModel;

Dotenv\Dotenv::createImmutable(__DIR__ . "/../")->load();

$redis = RedisModel::createMultiConnect();
$ips = [
    "http://169.57.1.85:8123",
    "http://51.195.19.131:80",
    "http://35.234.248.49:3128",
    "http://51.75.6.2:32648",
    "http://142.47.91.6:80",
];

foreach ($ips as $ip) {
    $redis->sadd("ips", $ip);
}